/*
 * =====================================================================================
 *
 *       Filename:  server.c
 *
 *    Description:  Demo server for blur filter calculation
 *
 *        Version:  1.0
 *        Created:  05/24/2014 08:21:03 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vaduva Jan Alexandru ()
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <math.h>


#define MAX_LINE 256
#define MAX_FILTER 5

#define PGM 1
#define PPM 3

#define CHECK(x)                                    \
    do {                                        \
        if (!(x)) {                                 \
            printf("[%s]:%s:%d: ", __FILE__,            \
            __func__, __LINE__);                        \
            perror(#x);                             \
            exit(EXIT_FAILURE);                         \
        }                                   \
    } while(0)

int **image;
int clients_no;
int imgN = 0, imgM = 0;
int maxc = 255, type = 0;
FILE *inImg, *outImg;

typedef struct tagFilter {
    int filter[3][3];  //matricea care retine filtrul
    int factor;
    int offset;
}FILTER;

typedef struct {
    int sock;
    struct sockaddr address;
    int addr_len;
} connection_t;


void printFilter(FILTER f) {
    printf("  %d %d %d\n", f.filter[0][0],f.filter[0][1],f.filter[0][2]);
    printf("  %d %d %d\n", f.filter[1][0],f.filter[1][1],f.filter[1][2]);
    printf("  %d %d %d\n", f.filter[2][0],f.filter[2][1],f.filter[2][2]);
    printf(" factor = %d, offset = %d",f.factor, f.offset);
}

void print(int **mat, int n, int m) {
    int i, j;
    printf("\n");
    for (i = 0; i<n; i++){
    for (j = 0; j<m; j++) printf(" %3d",mat[i][j]);
        printf("\n");
    }
}

void printout(FILE *out, int **mat, int n, int m, int max, int type) {
    int i, j;
    if (type == PGM) fprintf(out, "P2\n");
    if (type == PPM) { return; };
    fprintf(out,"%d %d\n%d\n",m, n, max);

    for (i = 0; i<n; i++){
    for (j = 0; j<m; j++)
        if (type==PGM) fprintf(out, "%d\n",mat[i][j]);
    }
}

void print_test(FILE *out, int **mat, int n, int m, int max, int type) {
    int i, j;
    for (i = 0; i<n; i++)
    for (j = 0; j<m; j++)
        fprintf(out, "%d\n", mat[i][j]);
}


int  filter(int i, int j, int imgM, FILTER f, int stripSize) {
    int val = 0; // val
    if ((i <= 1) || (i >= (stripSize)) || (j == 0) || (j == imgM-1)) return image[i][j];
    val += image[i-1][j] * f.filter[0][1];
    val += image[i  ][j] * f.filter[1][1];
    val += image[i+1][j] * f.filter[2][1];

    val += image[i-1][j-1] * f.filter[0][0]; // stanga jos
    val += image[i  ][j-1] * f.filter[1][0]; // stanga mijloc
    val += image[i+1][j-1] * f.filter[2][0]; // stanga sus

    val += image[i-1][j+1] * f.filter[0][2]; // dreapta jos
    val += image[i  ][j+1] * f.filter[1][2]; // dreapta mijloc
    val += image[i+1][j+1] * f.filter[2][2]; // dreapta sus

    val = (val / f.factor) + f.offset;
    if (val < 0) val = 0;
    if (val > 255) val = 255;

    return val;
}

void loadImage(FILE *in, int *n, int *m, int *maxc, int *type) {
    // image must have this format
        // PX - on the first line - where PX is P2-pgm or P3-ppm
        // some comment lines beginning  with #
        // x y - image size
        // max_color for P3 format
        // x * y * type   integer, where type = 1 for P2, type = 3 for P3
    // lines should be less than 256 chars
    // colors should have vallues betwen (0,0,0) and (255,255,255)

    char *s = (char *) calloc (MAX_LINE, sizeof(char));
    int i, j, k, val;

    fscanf(in, "%s\n", s);
    if (strncmp(s,"P2",2) == 0)
        (*type)=PGM;//P2
    else if (strncmp(s,"P3",2) == 0)
        (*type)=PPM;//P3
    else
        printf("Error: Unsuported image type %s\n",s);

    /* reading comment */
    fgets(s, 256, in);
    if (s[0] != '#') {
        sscanf(s, "%d %d\n", m, n);
    } else {
        fscanf(in,"%d %d\n", m, n);
    }

    fscanf(in,"%d", maxc);

    /* memory allocation */
    image = (int**) calloc ((*n) + 2, sizeof(int*));
    if (image == NULL )
        printf("Process 0 could not allocate any more memory\n");

    for (i = 1; i<(*n)+1; i++) {
        image[i] = (int*) calloc ((*m), sizeof(int));
        if (image[i] == NULL )
            printf("Process 0 could not allocate any more memory\n");
    }

    for (i = 1; i < (*n)+1; i++)
        for (j = 0; j < (*m); j++) {
            if ((*type) == 1) {
                fscanf(in,"%d",&val);
                image[i][j] = val;
            } else {
                image[i][j] = 0;
                /* store a non-gray scale value on a singular INT32 value 0x0 red green blue */
                for (k = 0; k < (*type); k++) {
                    fscanf(in,"%d",&val);
                    image[i][j] += val << (8 * (2 - k));
                }
            }
        }
}


FILTER initFilter(int type) {
    FILTER f;
    switch (type) {
        case 0: //identitate
            f.filter[0][0] = 0; f.filter[0][1] = 0; f.filter[0][2] = 0;
            f.filter[1][0] = 0; f.filter[1][1] = 1; f.filter[1][2] = 0;
            f.filter[2][0] = 0; f.filter[2][1] = 0; f.filter[2][2] = 0;
            f.factor = 1;
            f.offset = 0;
            break;
        case 1:  // smooth
            f.filter[0][0] = 1; f.filter[0][1] = 1; f.filter[0][2] = 1;
            f.filter[1][0] = 1; f.filter[1][1] = 1; f.filter[1][2] = 1;
            f.filter[2][0] = 1; f.filter[2][1] = 1; f.filter[2][2] = 1;
            f.factor = 9;
            f.offset = 0;
            break;
        case 2: //blur
            f.filter[0][0] = 1; f.filter[0][1] = 2; f.filter[0][2] = 1;
            f.filter[1][0] = 2; f.filter[1][1] = 4; f.filter[1][2] = 2;
            f.filter[2][0] = 1; f.filter[2][1] = 2; f.filter[2][2] = 1;
            f.factor = 16;
            f.offset = 0;
            break;
        case 3: //sharpen
            f.filter[0][0] = 0;  f.filter[0][1] = -2; f.filter[0][2] = 0;
            f.filter[1][0] = -2; f.filter[1][1] = 11; f.filter[1][2] = -2;
            f.filter[2][0] = 0;  f.filter[2][1] = -2; f.filter[2][2] = 0;
            f.factor = 3;
            f.offset = 0;
            break;
        case 4: //mean removal
            f.filter[0][0] = -1; f.filter[0][1] = -1; f.filter[0][2] = -1;
            f.filter[1][0] = -1; f.filter[1][1] =  9; f.filter[1][2] = -1;
            f.filter[2][0] = -1; f.filter[2][1] = -1; f.filter[2][2] = -1;
            f.factor = 1;
            f.offset = 0;
            break;
        default: //identitate
            f.filter[0][0] = 0; f.filter[0][1] = 0; f.filter[0][2] = 0;
            f.filter[1][0] = 0; f.filter[1][1] = 1; f.filter[1][2] = 0;
            f.filter[2][0] = 0; f.filter[2][1] = 0; f.filter[2][2] = 0;
            f.factor = 1;
            f.offset = 0;
            break;
    }
    return f;
}

void * process(void * ptr) {
    char * buffer;
    int len;
    connection_t * conn;
    long addr = 0;
    //char str1[]="Test-string";

    if (!ptr) pthread_exit(0);
    conn = (connection_t *)ptr;

    /* write to client the needed information */
    //write(conn->sock, str1, strlen(str1)+1);

    /**
     * TODO: De impartit matricea pe linii in functie de numarul de
     *  clienti pentru a fi facute prelucrarile.
     *      De asemenea trebuie apoi matricea sa fie reconstruita.
     *      In ultima etapa trebuie sa se calculeze timpi de procesare
     *  si sa fie afisati intr-un format usor de prelucrat grafic.
     * */

    /* read length of message */
    read(conn->sock, &len, sizeof(int));
    if (len > 0) {
        addr = (long)((struct sockaddr_in *)&conn->address)->sin_addr.s_addr;
        buffer = (char *)malloc((len+1)*sizeof(char));
        buffer[len] = 0;

        /* read message */
        read(conn->sock, buffer, len);

        /* print message */
        printf("%d.%d.%d.%d: %s\n",
            (int)((addr      ) & 0xff),
            (int)((addr >>  8) & 0xff),
            (int)((addr >> 16) & 0xff),
            (int)((addr >> 24) & 0xff),
            buffer);
        free(buffer);
    }

    /* close socket and clean up */
    close(conn->sock);
    free(conn);
    pthread_exit(0);
}

int main(int argc, char ** argv)
{
    int sock = -1;
    struct sockaddr_in address;
    int port;
    connection_t * connection;
    pthread_t thread;

    int i, j;
    FILTER f;

    int filterType = 0;
    char *s = (char*) calloc (MAX_LINE, sizeof(char));

    /* check for command line arguments */
    if (argc < 4) {
        fprintf(stderr, "Usage: %s   input_pgm_file   port  clients_no\n", argv[0]);
        return -1;
    }

    /* numele fisierului de intrare */
    inImg = fopen(argv[1], "rt");
    CHECK( inImg != NULL );

    /* tipul filtrului si incarcarea acestuia */
    filterType = 2;
    f = initFilter(filterType);

    /* numele fisierului de iesire */
    sprintf(s, "%s_f%d", argv[1], filterType);
    outImg = fopen(s, "wt");
    CHECK(outImg != NULL);

    /* incarcarea efectiva a imaginii in matricea inImg */
    loadImage(inImg, &imgN, &imgM, &maxc, &type);

    /*  test the image load */
    //printf("%d\n", image[1][1]);

    /* define the number of clients that will do the filter aplication */
    if (sscanf(argv[3], "%d", &clients_no) <= 0) {
        fprintf(stderr, "%s: error: wrong parameter: clients_no\n", argv[0]);
        return -2;
    }

    /* obtain port number */
    if (sscanf(argv[2], "%d", &port) <= 0) {
        fprintf(stderr, "%s: error: wrong parameter: port\n", argv[0]);
        return -3;
    }

    /* create socket */
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock <= 0) {
        fprintf(stderr, "%s: error: cannot create socket\n", argv[0]);
        return -4;
    }

    /* bind socket to port */
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *)&address, sizeof(struct sockaddr_in)) < 0) {
        fprintf(stderr, "%s: error: cannot bind socket to port %d\n", argv[0], port);
        return -5;
    }

    /* listen on port */
    if (listen(sock, 5) < 0) {
        fprintf(stderr, "%s: error: cannot listen on port\n", argv[0]);
        return -6;
    }
    printf("%s: ready and listening\n", argv[0]);
    while (1) {
        /* accept incoming connections */
        connection = (connection_t *)malloc(sizeof(connection_t));
        connection->sock = accept(sock, &connection->address, &connection->addr_len);
        if (connection->sock <= 0) {
            free(connection);
        }
        else {
            /* start a new thread but do not wait for it */
            pthread_create(&thread, 0, process, (void *)connection);
            pthread_detach(thread);
        }
    }
    fclose(outImg);

    for (i = 0; i < imgN; i++)
        free(image[i]);
    free(image);

    return 0;
}
