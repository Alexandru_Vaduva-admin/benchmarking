#Demo makefile
CC = gcc
CFLAGS = -g -ggdb

all: client server
client:
	 $(CC) $(CFLAGS) -o client client.c
server:
	 $(CC) $(CFLAGS) -o server server.c -lpthread
clean:
	rm -rf client server

run_server:
	./server dog.pgm  5678
run_client:
	./client localhost 5678 "Hello World"
