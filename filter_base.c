#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


#define MAX_LINE 256
#define MAX_FILTER 5

#define PGM	1
#define PPM	3

#define CHECK(x) 									\
	do { 										\
		if (!(x)) { 								\
			printf("[%s]:%s:%d: ", __FILE__,			\
			__func__, __LINE__); 						\
			perror(#x); 							\
			exit(EXIT_FAILURE); 						\
		} 									\
	} while(0) 

int **image; // global image, global label

typedef struct tagFilter{
	int filter[3][3];  // matricea care retine filtrul
	int factor;
	int offset;	
}FILTER;

void printFilter(FILTER f){
	printf("  %d %d %d\n", f.filter[0][0],f.filter[0][1],f.filter[0][2]);
	printf("  %d %d %d\n", f.filter[1][0],f.filter[1][1],f.filter[1][2]);
	printf("  %d %d %d\n", f.filter[2][0],f.filter[2][1],f.filter[2][2]);
	printf(" factor = %d, offset = %d",f.factor, f.offset);
}

void print(int **mat, int n, int m) {
    int i, j;
    printf("\n");
    for (i = 0; i<n; i++){
	for (j = 0; j<m; j++) printf(" %3d",mat[i][j]);
		printf("\n");  
    }
}

void printout(FILE *out, int **mat, int n, int m, int max, int type) {
    int i, j;
    if (type == PGM) fprintf(out, "P2\n"); 
    if (type == PPM) { return; };
    fprintf(out,"%d %d\n%d\n",m, n, max);

    for (i = 0; i<n; i++){
	for (j = 0; j<m; j++)
		if (type==PGM) fprintf(out, "%d\n",mat[i][j]);
    }
}

int  filter(int **image, int i, int j, int imgM, FILTER f, int stripSize){
	int val = 0; // val
	if ((i <= 1) || (i >= (stripSize)) || (j == 0) || (j == imgM-1)) return image[i][j];
	val += image[i-1][j] * f.filter[0][1];
	val += image[i  ][j] * f.filter[1][1];
	val += image[i+1][j] * f.filter[2][1];

	val += image[i-1][j-1] * f.filter[0][0];// stanga jos
	val += image[i  ][j-1] * f.filter[1][0]; // stanga mijloc
	val += image[i+1][j-1] * f.filter[2][0]; // stanga sus
	
	val += image[i-1][j+1] * f.filter[0][2]; // dreapta jos
	val += image[i  ][j+1] * f.filter[1][2]; // dreapta mijloc
	val += image[i+1][j+1] * f.filter[2][2]; // dreapta sus
	
	val = (val / f.factor) + f.offset; 
	if (val < 0) val = 0;
	if (val > 255) val = 255;

	//if (val == 0) printf("%d-%d val=%d  valnext=%d filter[1][1]=%d\n",
	//		i,j,val,(int)((float)val/ f.factor) + f.offset, f.filter[1][1]);
	return val;
}

void loadImage(FILE *in, int *n, int *m, int *maxc, int *type){

	// image must have this format
		// PX - on the first line - where PX is  P2-pgm or  P3-ppm 
		// some comment lines beginning  with #
		// x y - image size
		// max_color for P3 format 
		// x * y * type   integer, where type = 1 for P2, type = 3 for P3
	// lines should be less than 256 chars
	// colors should have vallues betwen (0,0,0) and (255,255,255)

	char *s = (char *) calloc (MAX_LINE, sizeof(char));
	int i, j, k, val;

	fscanf(in, "%s\n", s);
	//unsigned int val;
	if (strncmp(s,"P2",2) == 0) 
		(*type)=PGM;//P2
	else if (strncmp(s,"P3",2) == 0) 
		(*type)=PPM;//P3
	else {
		printf("Error: Unsuported image type %s\n",s);
		//MPI_Abort(MPI_COMM_WORLD, 7777);
	}

	// reading comment
	fgets(s, 256, in);
	if (s[0] != '#'){
		sscanf(s, "%d %d\n", m, n);
	} else {
		fscanf(in,"%d %d\n", m, n);
	}

	fscanf(in,"%d", maxc);

	// memory allocation
	image = (int**) calloc ((*n) + 2, sizeof(int*));
	if (image == NULL ) {
		printf("Process 0 could not allocate any more memory\n");
	//	MPI_Abort(MPI_COMM_WORLD, 7777);
	}
	
	for (i = 1; i<(*n)+1; i++) {
		image[i] = (int*) calloc ((*m), sizeof(int));
		if (image[i] == NULL ) {
			printf("Process 0 could not allocate any more memory\n");
		}
	}

	for (i = 1; i < (*n)+1; i++)
	for (j = 0; j < (*m); j++)
	{
		if ((*type) == 1) {
			fscanf(in,"%d",&val);
			image[i][j] = val;
		} else {
			image[i][j] = 0;
			// store a non-gray scale value on a singular INT32 value
			// 0 x 0 red green blue 
			for (k = 0; k < (*type); k++){
				fscanf(in,"%d",&val);
				image[i][j] += val << (8 * (2 - k));
			}
	       }
	}	
}


FILTER initFilter(int type){
	FILTER f;
	switch (type) {
		case 0: //identitate
			f.filter[0][0] = 0;	f.filter[0][1] = 0; f.filter[0][2] = 0;
			f.filter[1][0] = 0;	f.filter[1][1] = 1; f.filter[1][2] = 0;
			f.filter[2][0] = 0;	f.filter[2][1] = 0; f.filter[2][2] = 0;
			f.factor = 1;
			f.offset = 0;
			break;
		case 1:  // smooth
			f.filter[0][0] = 1;	f.filter[0][1] = 1; f.filter[0][2] = 1;
			f.filter[1][0] = 1;	f.filter[1][1] = 1; f.filter[1][2] = 1;
			f.filter[2][0] = 1;	f.filter[2][1] = 1; f.filter[2][2] = 1;
			f.factor = 9;
			f.offset = 0;
			break;
		case 2: //blur
			f.filter[0][0] = 1;	f.filter[0][1] = 2; f.filter[0][2] = 1;
			f.filter[1][0] = 2;	f.filter[1][1] = 4; f.filter[1][2] = 2;
			f.filter[2][0] = 1;	f.filter[2][1] = 2; f.filter[2][2] = 1;
			f.factor = 16;
			f.offset = 0;	
			break;
		case 3: //sharpen
			f.filter[0][0] = 0;	f.filter[0][1] = -2; f.filter[0][2] = 0;
			f.filter[1][0] = -2;	f.filter[1][1] = 11; f.filter[1][2] = -2;
			f.filter[2][0] = 0;	f.filter[2][1] = -2; f.filter[2][2] = 0;
			f.factor = 3;
			f.offset = 0;	
			break;
		case 4: //mean removal
			f.filter[0][0] = -1;	f.filter[0][1] = -1; f.filter[0][2] = -1;
			f.filter[1][0] = -1;	f.filter[1][1] =  9; f.filter[1][2] = -1;
			f.filter[2][0] = -1;	f.filter[2][1] = -1; f.filter[2][2] = -1;
			f.factor = 1;
			f.offset = 0;	
			break;
		default: //identitate
			f.filter[0][0] = 0;	f.filter[0][1] = 0; f.filter[0][2] = 0;
			f.filter[1][0] = 0;	f.filter[1][1] = 1; f.filter[1][2] = 0;
			f.filter[2][0] = 0;	f.filter[2][1] = 0; f.filter[2][2] = 0;
			f.factor = 1;
			f.offset = 0;
			break;
	}
	return f;
}


/**
	@fisier_intrare
	@tip_filtru
	[@fisier_iesire] - optional
*/

int main( int argc, char **argv){
	
	int i, j;
	int imgN = 0, imgM = 0; 
	FILTER f;
	
	int filterType = 0;
	char *s = (char*) calloc (MAX_LINE, sizeof(char));
	int maxc = 255, type = 0;

	FILE *inImg, *outImg;

	// reading cmdline args
	if (argc < 3) {
		fprintf(stderr, "Usage: %s   input_pgm_file   filter_type[0-3]  [output_pgm_file]\n", argv[0]);
		fprintf(stderr, "Filter types \n\t0 - identitate \n\t1 - smooth\n\t2 - blur\n\t3 - sharpen\n\t4 - mean removal\n ");
		exit(0);
	}

	// numele fisierului de intrare
	inImg = fopen(argv[1], "rt");
	CHECK( inImg != NULL );
	
	// tipul filtrului si incarcarea acestuia
	filterType = atoi(argv[2]);
	if (filterType < 0 || filterType > MAX_FILTER) {
		fprintf(stderr, "Usage: %s   input_pgm_file   filter_type[0-3]  [output_pgm_file]\n", argv[0]);
		exit(0);
	}
	f = initFilter(filterType);
	
	// numele fisierului de iesire 
	if (argc == 3) { // definit la intrare
		sprintf(s, "%s_f%d", argv[3], filterType);
		outImg = fopen(s, "wt");
		CHECK(outImg != NULL);
	} else { // creat default		
		outImg = fopen(argv[3], "wt");
		CHECK(outImg != NULL);
	}
	
	// incarcarea efectiva a imaginii in matricea inImg
	loadImage(inImg, &imgN, &imgM, &maxc, &type);
	printf("%d\n", image[1][1]);

	/**
			Procesarea efectiva a imaginii prin aplicarea filtrului
	*/
	
	int **newimg = (int**) calloc (imgN+2, sizeof(int*)); 
    for (i=0; i<imgN+2; i++)
		newimg[i] = (int*) calloc(imgM, sizeof(int));
	 
    for (i=1; i<imgN + 1; i++) {
		for (j=0; j<imgM; j++) {
			newimg[i][j] = filter(image, i, j, imgM, f, imgN); 
		}
	}
	
    printout(outImg, newimg, imgN, imgM, maxc, type);
	fclose(outImg);

	for (i = 0; i < imgN; i++){
		free(image[i]);
		free(newimg[i]);
	}
	free(image);
	free(newimg);
	
	return 0;
}
